

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;



import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import controller.Job;
import controller.JobManager;

/**
 * Servlet implementation class Index
 */
@WebServlet("/Index")
public class Index extends HttpServlet{
	
	 private static final String UPLOAD_DIRECTORY = "upload";		 //local instance storage
	 private static final int THRESHOLD_SIZE     = 1024 * 1024 * 3;  // 3MB
	 private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
	 private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB
	 private static final long serialVersionUID = 1L;
	 private JobManager MANAGER = new JobManager();					 //build controller instance

	 /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        System.out.println("building socket");
        new Thread(MANAGER).start();	// start controller 
        								// needs inject of IP address before beginning
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
    	//Do get handles COMMANDS from ajax including
    	//getJobs -> will extend to get services information
    	//upload single IP address
  /*	
    	response.setContentType("application/json");//set response type 
    	response.setCharacterEncoding("utf-8");
    	JSONObject send = new JSONObject();			//build response target
    	
    	String handler = request.getParameter("COMMAND");//find request purpose
    	
    	if(handler.equals("getJobs")){ //update all local field variables 
    		getJobsInfo(send);		   //as JSONObjects can be expanded easily 
    								   //the extensions to server info will be 
    								   //straight forward 	
    	} else if (handler.equals("addIPAddress")){ //upload single ip address
    		addIPAddress(request.getParameter("ipAddress"));
    	} else {
    		
    	}
   
    	PrintWriter out = response.getWriter();		//output back to client window
		out.println(send);
	*/
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doPost handles all of the file retieval from client window - inclusive of 
		//jobs submissions
		//ip address submissions via <name>.ipset
		
/*		
		DiskFileItemFactory factory = new DiskFileItemFactory();//initialize blank template holder for properties of uploaded files 
		factory.setSizeThreshold(THRESHOLD_SIZE);// maximum size that will be stored in memory
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(MAX_REQUEST_SIZE);
		upload.setSizeMax(MAX_FILE_SIZE);// maximum size before a FileUploadException will be thrown
		
		try {	//try pulling information from request script
			String uploadPath = getServletContext().getRealPath("")
				    + File.separator + UPLOAD_DIRECTORY;
				// creates the directory if it does not exist
				File uploadDir = new File(uploadPath);
				if (!uploadDir.exists()) {
				    uploadDir.mkdir();
				}
				
			List fileItems = upload.parseRequest(new ServletRequestContext(request));
			Iterator i = fileItems.iterator();
			List<File> files = new ArrayList<File>(); 
			while ( i.hasNext() ){
				FileItem fi = (FileItem)i.next();
				if ( !fi.isFormField() ){
		        	String fileName = new File(fi.getName()).getName();// Get the uploaded file parameters	
		        	String filePath = uploadPath + File.separator + fileName;
	                files.add(new File(filePath));
	                fi.write(new File(filePath));// saves the file on local instance
		        }
		    }
			if(files.size()==2){
				MANAGER.uploadJob( new Job( files.get(0),files.get(1),MANAGER.getNumberOfJobs()+1));
			} else if(files.size()==1 && files.get(0).getName().contains(".ipset")){
				List<String> ip = new ArrayList<String>(); 
				try (BufferedReader br = new BufferedReader(new FileReader(files.get(0)))) {
					for(String line; (line = br.readLine()) != null; ) {
						if(line.matches("^[0-9]{3}.[0-9]{3}.[0-9]{1}.[0-9]{1}$")){
				    	  ip.add(line);
						}
				    }
					if(!ip.isEmpty()){
						MANAGER.uploadIPAddresses(ip);
					}
				} catch (Exception ex) {
					System.err.println("caught error: " + ex.getMessage());
				}
				ip = null;   //for garbage collector
			} else {
				
			}
			files = null;	 //for garbage collector
			i = null;		 //for garbage collector
		    fileItems = null;//for garbage collector
        } catch (Exception ex) {
            System.out.println("message: There was an error: " + ex.getMessage());
		} finally {
			factory = null;
			upload = null;	
			response.sendRedirect("/Master");
		}
	*/
	}
	private void getJobsInfo(JSONObject send){
    	send.put("numberOfJobs", MANAGER.getNumberOfJobs());
    	JSONArray jsonArray = new JSONArray();
    	for(int i=0; i<MANAGER.getNumberOfJobs();i++){
    		System.out.println("printing response");
    		JSONObject jo = new JSONObject();
    		jo.put("jarName",MANAGER.getJarName(i));	//jarName
    		jo.put("inputName",MANAGER.getInputName(i));//inputName
    		jo.put("jobNumber",MANAGER.getJobNumber(i));//jobNumber
    		jo.put("jobStatus",MANAGER.getJobStatus(i));//jobStatus
    		jsonArray.add(jo.toJSONString());
    	}
    	send.put("jobs",jsonArray);  	
	}
	private void addIPAddress(String string){
		if(string.matches("^[0-9]{3}.[0-9]{3}.[0-9]{1}.[0-9]{1}$")){
			MANAGER.uploadIPAddress(string);
	    }
	}
}