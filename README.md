# COMP90015 Distributed Systems Assignment 2

This README should serve to document the contents, procedures and instructions of the contained projects.

# Team Members

**Joshua Wilkosz** (326537)

**Alex Gross** (734266)

**Siftnoor Singh** (740526)

**Jeffery Hollingworth** (358660)


# Eclipse IDE - MASTER

### Dependancies for running MASTER located in Appendices#Dependecies 

##Usage

1. run MASTER on tomcat server (may need to create see Appendix#Dependices) 

..1.1 may need to create server - see Appendix#Dependices

..1.2 add Master/lib jars to both build path and Deployment Assembly 

2. submitting ipAddress completed via file <Name>.ipset which has one ipAddress per line, e.g. 

192.168.0.2

192.168.0.1

192.168.0.0

3. single ip Addresses can be submitted and the JobMnanager will update the working List (no connections as of yet)

# Eclipse IDE - CLIENT


# Nectar Instance
For the clients, use the following settings to setup the initial instance.

## Initial Setup

### 1. Security Group Rules

Setup a security group to be added to the instance which allows communication on the following ports:
* Ingress TCP 22
* Ingress TCP 4000-4100 

### 2. Key Pairs

You can find a public key in the `/ssh_key/comp90015-a2.rsa.pub` file for use with the VM's. Import this key into your nectar Key Pairs tab.

You will also find the private key as `/ssh_key/comp90015-a2.rsa` If you wish, you can add this to your ssh-agent, instructions [here](https://help.github.com/articles/generating-ssh-keys/) or provide at connection on the command line, etc.

For the putty users, use the `/ssh_key/comp90015-a2.rsa.ppk` under the `Connection/SSH/Auth` tab as the `Private key file for authentication`

### 3. Instance

* Flavor: m2.xsmall
* Image Name: NeCTAR Ubuntu 14.10 (Utopic) amd64
* Security Groups: *setup in step 1*
* Key Pair: *setup in step 2*

### 4. After First Boot

Run the set of shell commands to install java and other dependencies

```
sudo add-apt-repository ppa:webupd8team/java -y
sudo apt-get update
sudo apt-get install -y oracle-java8-installer
 
sudo apt-get update
sudo apt-get install -y oracle-java8-installer
sudo update-java-alternatives -s java-8-oracle
sudo apt-get install -y oracle-java8-set-default
sudo apt-get install -y vim
```

### 5. Create a snapshot

Create a snapshot and use this as the base for all server instances. If you need to add other dependencies, update the instructions above and inform other group members of changes.

#Appendices

##Dependices

###Dependices MASTER

Dependancies package for 'dynamic web project' 

_Tutorial java sevlets here_ (http://www.vogella.com/tutorials/EclipseWTP/article.html)

_Tutorial Apache tomcat here_ (http://crunchify.com/step-by-step-guide-to-setup-and-install-apache-tomcat-server-in-eclipse-development-environment-ide/)

_Tutorial Java servelet here__ (http://www.journaldev.com/1854/java-web-application-tutorial-for-beginners#servlets-jsps)

Dependencies package for dynamic web project grouped under Web, XML, Java EE and OSGI Enterprise Development, inclusive of 


1. CXF Web Services

2. Eclipse Java Web Development Tools

3. Eclipse Web Development Tools

4. JSF Tools - Web Page Editor

General Purpose Tools	

1. SWT Designer XWT Support (requires Eclipse WTP/WST)	1.7.0.r44x201405021548

2. WindowBuilder XML Core (requires Eclipse WTP/WST)	1.7.0.r44x201405021458
 
Web, XML, Java EE and OSGi Enterprise Development	

1. m2e-wtp - JAX-RS configurator for WTP (Optional)	1.1.0.20140611-1646
    
2. m2e-wtp - JPA configurator for WTP (Optional)	1.1.0.e43-20140611-1648

3. m2e-wtp - JSF configurator for WTP (Optional)	1.1.0.20140611-1646

4. m2e-wtp - Maven Integration for WTP	1.1.0.20140611-1646

5. JST Server Adapters	3.2.301.v201410241731

6. JST Server Adapters Extensions	3.3.302.v201501040116

7. JST Server UI	3.4.101.v201410241731
