# Operations 

## Master VM Arguements

You will need to run the master instance with the following VM Arguements to add the corrosponding SSL certificates to the JVM TrustStore.

```
-Djavax.net.ssl.trustStore=/path/to/comp90015a2-cacerts.jks -Djavax.net.ssl.trustStorePassword=comp90015a2pword
```

## Worker VM Arguements

When running the worker locally you will need to add the following VM Arguements to add the corrosponding SSL certificates to the JVM KeyStore.

```
-Djavax.net.ssl.keyStore=/path/to/comp90015a2-keystore.jks -Djavax.net.ssl.keyStorePassword=comp90015a2pword
```

## Using the deploy_worker.sh Script

This script is there to aid in the deployment, execution and status reporting of the executable worker.jar file onto on of the NeCTAR (or similiarly configured) servers.

```
sh manage_worker.sh [deploy|status] [ip_addr]
```

## Create an SSL keyset

Enter the command below to generate both the public and private java ssl key store and trust stores. **Note** this has already been done for the project and reside in this file.

```
java-home\bin\keytool -genkey -alias comp90015a2Server -keyalg RSA -keypass comp90015a2pword -storepass comp90015a2pword -keystore comp90015a2-keystore.jks
```

```
java-home\bin\keytool -export -alias comp90015a2Server -storepass comp90015a2pword -file comp90015a2-server.cer -keystore comp90015a2-keystore.jks
```

```
java-home\bin\keytool -import -v -trustcacerts -alias comp90015a2Server -file comp90015a2-server.cer -keystore comp90015a2-cacerts.jks -keypass comp90015a2pword -storepass comp90015a2pword
```
