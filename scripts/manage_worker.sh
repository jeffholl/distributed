#! /bin/bash

if [ $1 = 'deploy' ]; then

	if [ -z "$2" ]; then
		echo "enter the host address";
		exit 1;
	fi

	echo "deploying worker.jar to host at $2";

	scp ./worker.jar ubuntu@$2:~/worker.jar

	echo "deploying comp90015a2-keystore.jks to host at $2";

	scp ./comp90015a2-keystore.jks ubuntu@$2:~/comp90015a2-keystore.jks

	echo "stopping old servers"

	ssh ubuntu@$2 screen -X -S worker80 quit

	ssh ubuntu@$2 "echo \"\" > screenlog.0"

	echo "starting worker";

	ssh ubuntu@$2 'screen -dmSL worker80 sh -c "sudo java -Djavax.net.ssl.keyStore=/home/ubuntu/comp90015a2-keystore.jks -Djavax.net.ssl.keyStorePassword=comp90015a2pword -jar /home/ubuntu/worker.jar -p 80; exec /bin/bash"'

	echo "waiting for worker to boot"

	sleep 3

	echo "worker stdout"

	ssh ubuntu@$2 'tail -20 screenlog.0'

elif [ $1 = 'status' ]; then

	if [ -z "$2" ]; then
		echo "enter the host address";
		exit 1;
	fi

	echo "screen status"

	ssh ubuntu@$2 'screen -ls | grep -i worker80'

	echo "stdout"

	ssh ubuntu@$2 'tail -20 screenlog.0'

else

	echo "command not recognised";

fi
