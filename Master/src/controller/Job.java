package controller;

import java.io.File;

public class Job {
	
	private String jar = null;		   //name jar
	private String input = null;       //name input
	private String output = null;      //name output 
	private String path = null;        //path to either upload or download
	private String download = null;    //'download' Directory name
	private String upload = null;      //'upload' Directory name  
									   // to get full path requires: path + File.separator + upload etc... 
	private int runTime = 0;
	private int ramAllocation = 0;
	private int status;
	private int jobNumber = 0;
	

	public Job (int ID, String status){
		jobNumber = ID;
		this.setStatus(status);
	}
	
	public Job (String jar, String inputFile, int number, String path, String uploadFile, String downloadFile, int run,int ram){
		this.jar = jar;
		this.input = inputFile;
	    this.status = 0;
	    this.jobNumber= number;
	    this.path = path;
	    this.upload = uploadFile;
	    this.download = downloadFile;
	    this.runTime = run;
	    this.ramAllocation = ram;
	}	
	
	public void setOutputName(String outpout){
		this.output = outpout;
	}
	public String getDownload(){
		return download;
	}
	public String getUpload(){
		return upload;
	}
	public void setJobNumber(int i){
		jobNumber = i;
	}
	public Integer getJobID (){
		return jobNumber;
	}
	public String getJarName(){
		return jar;
	}
	public String getInputName(){
		return input;
	}
	public String getPath(){
		return path;
	}
	public void SetOutputComplete(String _name){
		output = _name;
		status = 2;
	}
	public String getOutputName(){
		return output;
	}
	public int getJobNumber(){
		return jobNumber;
	}
	public String getJobStatus(){
		String send;
		switch (status){
			case 0: send="queued";
					break;
			case 1: send="running";
					break;
			case 2: send="completed";
					break;
			case 3: send="cancelled";
			 		break;
			case 4: send="error";
	 				break;
			case 5: send="disabled";
					break;
			case 6: send="timeout";
					break;
			case 7: send="download";
					break;
			case 8: send="downloaderror";
					break;
			default:send="unknown";
			 		break;
		}	
		return send;
	}
	public void setStatus(String string){
		switch (string){
		case "queued": status=0 ;
				break;
		case "completed": status = 2;
				break;
		case "running": status = 1;
				break;
		case "cancelled": status = 3;
		 		break;
		case "error": status = 4;
				break;
		case "disabled": status = 5;
				break;
		case "timeout": status = 6;
				break;
		case "download": status = 7;
				break;
		case "downloaderror": status = 8;
				break;
		default:
		 		break;}
	}	
	public boolean deleteJob(){
		status = 3;
		return true;
	}
	public boolean setStatus(int _int){
		status = _int;
		return true;
	}
	public int getRunTime(){
		return runTime;
	}
	public int getRamAllocation(){
		return ramAllocation;
	}
	public File getJarFile(){
		return new File(this.getPath() + this.getUpload() + File.separator + this.getJarName() );
	}
	public File getInputFile(){
		return new File(this.getPath() + this.getUpload() + File.separator + this.getInputName() );
	}
}
