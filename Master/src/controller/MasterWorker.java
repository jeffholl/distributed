package controller;

import java.awt.JobAttributes;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class MasterWorker {
	

	private String status = "down";
	private Socket socket;
	//This number of Jobs represents the number of TOTAL jobs of the worker (also from other masters)
	private int noOfJobs;
	private Integer workerID;

	//This List of jobs contains ONLY the jobs from the Master that created the instance
	private HashMap<Integer, Job> jobs;
	private int cpuUsage;

	//Hard code Filesize - maybe adapt? 100mb failing at 500mb
	public final static int FILE_SIZE = 100223869;
	
	public MasterWorker(Integer workerID, Socket socket){
		System.out.println("connected to Worker at "+socket.getInetAddress()+":"+socket.getPort());
		this.socket = socket;
		this.workerID = workerID;
		this.jobs = new HashMap<Integer, Job>();
		this.noOfJobs = getNoOfJobs();


		getNoOfJobs();
		isAlive();

	}
	
	public Integer getWorkerID() {
		return this.workerID;
	}
	
	public String getStatus(){
		isAlive();
		return status;
	}

	private void isAlive() {
			synchronized(socket){
			try {
				JSONObject requestStatus = new JSONObject();
				requestStatus.put("Type", "STATUS");
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());
				DataInputStream in = new DataInputStream(socket.getInputStream());
				out.writeUTF(requestStatus.toJSONString());
				
				String message = in.readUTF();
				JSONParser parser = new JSONParser();
				JSONObject messageJSON = (JSONObject) parser.parse(message);
				
				if(messageJSON.get("Type").toString().equals("Alive")){
					status = "running";
				}
				else{
					status = "down";
				}
			} catch (IOException e) {
				
				status = "down";
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
	}



	/**
	 * 
	 * @param job
	 * @return
	 * @throws IOException 
	 */
	public void deleteJob(Job job){
		
		JSONObject requestOutput = new JSONObject();
		requestOutput.put("Type", "DELETE-JOB");
		requestOutput.put("JOB-ID", job.getJobNumber());
			DataOutputStream out;
			synchronized(socket){
			try {
				out = new DataOutputStream(socket.getOutputStream());
				out.writeUTF(requestOutput.toJSONString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Worker is down");
				status = "down";
			}
		}

		jobs.remove(job.getJobID());
		
	}
	public void updateJobs(){
		try {
			synchronized(socket){
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			DataInputStream in = new DataInputStream(socket.getInputStream());
			JSONObject requestJobs = new JSONObject();
			requestJobs.put("Type", "GET-JOBS");
			
			System.out.println("Master sends message to update jobs: "+requestJobs.toJSONString());
			out.writeUTF(requestJobs.toJSONString());
			
			String message = in.readUTF();
			System.out.println("message after GET-JOBS");
			System.out.println(message);
			JSONParser parser = new JSONParser();
			JSONObject messageJSON = (JSONObject) parser.parse(message);
			
			JSONArray jobIDs = (JSONArray) messageJSON.get("IDs");
			JSONArray jobStatuses = (JSONArray) messageJSON.get("Statuses");
			System.out.println("Message of worker received, number of jobs = "+jobIDs.size());
			
			status = "running";
			int numberOfActiveJobs = 0;
			int jobID;
			String status;
			for(int i = 0; i<jobIDs.size(); i++){
				jobID = Integer.valueOf(jobIDs.get(i).toString());
				status = jobStatuses.get(i).toString();
				if (status.equals("running") || status.equals("queued")){
					numberOfActiveJobs++;
				}
				
				boolean found = false;
							
				for(HashMap.Entry<Integer, Job> jobEntry : this.jobs.entrySet()){
					Job job = jobEntry.getValue();
					if(jobID == job.getJobNumber()){
						System.out.println("Job found in this master, sent status: "+status);
						if(status.equals("completed")){
							System.out.println("Totally not an error - job completed, now contacting worker to send it");
							this.getOutPutFile(job, socket);
							job.setStatus("download");
							found=true;
							break;
						}else if (status.equals("error")){
							System.out.println("Job Error, retrieving error file");
							this.getOutPutFile(job, socket);
							job.setStatus("downloaderror");
							found=true;
							break;
						}else{
							System.out.println("Job not completed or error, no job download atm");
							job.setStatus(status);
							found = true;
							break;}
					}
				}
				if (found == false){
					Job job = new Job(jobID, "disabled");
					jobs.put(job.getJobID(),job);
					System.out.println("Job from another master found and added");
				}
			
				
			}
			noOfJobs = numberOfActiveJobs;
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Worker is down");
			status = "down";
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	public HashMap<Integer, Job> getJobs(){

		return jobs;
	}
	/**
	 * Asks the worker to tell how many jobs are queued or running (not completed or error, as these don't require resources)
	 * 
	 */
	public int getNoOfJobs(){
		this.updateJobs();
		return noOfJobs;
	}
	
	public String getIPAddress () {
		return socket.getInetAddress().toString() + ":" + socket.getPort();
	}
	
	/**
	 * will ask the worker to send the outputfile and stores the outputfile and sets the path and name into the job.
	 * Returns true if job completed, false if error, running or whatever else
	 * @param job
	 * @return
	 * 0 = no problems, output file created
	 * 1 = still running, no file created
	 * 2 = errors, error stream in output file
	 * 3 = something strange happened.
	 */
	public int getOutPutFile(Job job, Socket asocket){
		JSONObject requestOutput = new JSONObject();
		requestOutput.put("Type", "GET-JOB-DATA");
		requestOutput.put("JOB-ID", job.getJobNumber());
		
		try {
			DataOutputStream out = new DataOutputStream(asocket.getOutputStream());
			DataInputStream in = new DataInputStream(asocket.getInputStream());
			
			out.writeUTF(requestOutput.toJSONString());
			
			String message = in.readUTF();
			JSONParser parser = new JSONParser();
			JSONObject messageJSON = (JSONObject) parser.parse(message);
			
			if (messageJSON.get("Type").toString().matches("JOB-RUNNING")){
				System.out.println("Can't return output file because job running");
				return 1;
			} else if (messageJSON.get("Type").toString().matches("ERROR")){
				System.out.println("Can't return output file because error. Trying to receive Errorfile instead");
				
				int fileSize = Integer.valueOf(messageJSON.get("FILESIZE").toString());
				System.out.println("Trying to receive Error file from worker");

				//Should create a File in the Download Directory
				getFile(job, fileSize, asocket);
				job.setStatus("downloaderror");
				return 2;
			} else {
				int fileSize = Integer.valueOf(messageJSON.get("FILESIZE").toString());
				System.out.println("Trying to receive output file from worker");

				//Should create a File in the Download Directory
				getFile(job, fileSize, asocket);
				job.setStatus("download");
				return 0;
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Worker is down");
			status = "down";
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 3;
		
	}
	
	/**
	 * returns true if cancelled successfully
	 * @param job
	 * @return
	 * @throws IOException 
	 */
	public void cancelJob(Job job){
		JSONObject requestOutput = new JSONObject();
		requestOutput.put("Type", "CANCEL-JOB");
		requestOutput.put("JOB-ID", job.getJobNumber());
		synchronized(socket){
			DataOutputStream out;
			try {
				out = new DataOutputStream(socket.getOutputStream());
				out.writeUTF(requestOutput.toJSONString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Worker is down");
				status = "down";
				e.printStackTrace();
			}
		}
		job.setStatus("cancelled");
		System.err.println("Job Cancelled successfully");
	}
	
	public void removeJob(Integer jobID){
		jobs.remove(jobID);
	}	
	
	@SuppressWarnings("unchecked")
	public void createJob(Job job){
		System.out.println("creating a job");
		try{
			synchronized(socket){
			System.out.println("start try statement");
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			DataInputStream in = new DataInputStream(socket.getInputStream());
			System.out.println("");
			JSONObject sendNewJob = new JSONObject();
			System.out.println("Type");
			sendNewJob.put("Type", "CREATE-JOB");
			System.out.println("JAR");
			sendNewJob.put("JAR-FILENAME", job.getJarName());
			System.out.println("INPUT");
			sendNewJob.put("INPUT-FILENAME", job.getInputName());
			System.out.println("JAR");
			sendNewJob.put("JAR-SIZE", job.getJarFile().length());
			System.out.println("INPUT");
			sendNewJob.put("INPUT-SIZE", job.getInputFile().length());
			System.out.println("MAX");
			sendNewJob.put("MAX-RAM", job.getRamAllocation());
			System.out.println("DEADLINE");
			sendNewJob.put("DEADLINE", job.getRunTime());
			System.out.println("building json header");
			out.writeUTF(sendNewJob.toJSONString());
			out.flush();
			System.out.println("send header");
			{
				String message = in.readUTF();
				JSONParser parser = new JSONParser();
				JSONObject messageJSON = (JSONObject) parser.parse(message);
				
				
				
				//more or less twice the same code. Change
				if(!messageJSON.get("Type").toString().matches("sendJar")){
					System.out.println("Client sent wronge mssage");
				} else {
					/*
					 * Send the jar-File to the worker
					 */
					job.setJobNumber(Integer.valueOf(messageJSON.get("JOB-ID").toString()));
					jobs.put(job.getJobID(),job);
					System.out.println(" New Job created -> Sending Jar File");
					//out.writeUTF("Sending File");
					sendFile(job.getJarFile(), socket);
				}
			}
			
			String message = in.readUTF();
			JSONParser parser = new JSONParser();
			JSONObject messageJSON = (JSONObject) parser.parse(message);
			
			if(!messageJSON.get("Type").toString().matches("sendInput")){
				System.out.println("Client sent wronge mssage");
			} else {
				/*
				 * Send the Input to the worker
				 */
				//out.writeUTF("Sending File");
				sendFile(job.getInputFile(), socket);
			}
			
			
			//in.readUTF();
			
			
			}
			}catch (IOException e){
				System.out.println("Worker is down");
				status = "down";
				e.printStackTrace();} catch (ParseException e) {
				// TODO Auto-generated catch block
					
				e.printStackTrace();
			}

		
	}
	
	private void sendFile(File file, Socket asocket){
		FileInputStream fis = null;
	    BufferedInputStream bis = null;
	    OutputStream os;
	    
	        byte [] mybytearray  = new byte [(int) file.length()];
	        try {
				fis = new FileInputStream(file);
				bis = new BufferedInputStream(fis);
		        bis.read(mybytearray,0,mybytearray.length);
		        os = asocket.getOutputStream();
		        os.write(mybytearray,0,mybytearray.length);
		        os.flush();
		        System.out.println("Sent File to Worker");
		        
		        if (bis != null) bis.close();
		        if (fis != null) fis.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Worker is down");
				status = "down";
				e.printStackTrace();
			}
	        
	        
	}
	
private File getFile(Job job, int filesize, Socket asocket){
		
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		System.out.println("creating new File for touput");
		
		File received = new File(job.getPath() + job.getDownload() + File.separator + buildOutPutFile(this.socket.getInetAddress().toString(),job.getJobID(),job.getJarName()));

		try {
			int bytesRead;
			int current = 0;
			InputStream is;
			System.out.println("start a socket");
			is = asocket.getInputStream();
			byte [] mybytearray  = new byte [FILE_SIZE];
			fos = new FileOutputStream(received);
		    bos = new BufferedOutputStream(fos);
		    System.out.println("waiting for a socket");
		    bytesRead = is.read(mybytearray,0,mybytearray.length);
//		    int BufferSize = workerSocket.getReceiveBufferSize();
		    current = bytesRead;
		    System.out.println("First read of File Complete");
		    
		      while(current<filesize) {
		         bytesRead = is.read(mybytearray, current, (mybytearray.length-current));
		         if(bytesRead >= 0) current += bytesRead;
		         System.out.println("Do-loop entered and bytes read"+bytesRead);
		      } 
		      
		      //jobs.get(job.getJobNumber()).SetOutputComplete(buildOutPutFileName(jobs.get(job.getJobNumber()).getJarName()));
		      bos.write(mybytearray, 0 , current);
		      bos.flush();
		      System.out.println("Output File Received");
		     
	      
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Worker is down");
			status = "down";
		}finally {
			if (fos != null)
				try {
					fos.close();
					 if (bos != null) bos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		   
		}
	      return received;
	}
	
public String getJobOutputName(Job job){
	String ipaddf = "f";
	String ipadd=this.socket.getInetAddress().toString();
	String triplets[]=ipadd.split(java.util.regex.Pattern.quote("."));
	for(int i=0;i<triplets.length;i++){
		if(triplets[i].length()==1){
			triplets[i]="00"+triplets[i];
		}
		if(triplets[i].length()==2){
			triplets[i]="0"+triplets[i];
		}
		ipaddf+=triplets[i];
	}

	int jID=job.getJobNumber();
	String name=job.getOutputName();
	String filename=ipaddf+jID+name;
	return filename;
}
	
	private String buildOutPutFileName(String _name){
		String[] parts = _name.split("\\.");
		return parts[0] + "-output.txt";
	}
	//spool down server from user input
	public boolean shutDown(){			
		boolean complete = true;
		// set server status to down
		this.jobs.clear();
		try {
			socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			complete = false;
		}
		return complete;
	}
	public int getUsage(){
		return noOfJobs;
	}
	public String getIPAddr() {
		return this.socket.getRemoteSocketAddress().toString();
	}
	public String buildOutPutFile(String _string, int _int, String _jar){
		String[] parts = _jar.split("\\.");
		System.out.println(parts[0]);
		String ip = _string.replaceAll("\\.","");
		String[] ips = ip.split("/");
		System.out.println(ip);
		jobs.get(_int).setOutputName(ips[1]+_int+parts[0]+".txt");
		//strore output string in job file
		return ips[1]+_int+parts[0]+".txt";
	}
}


