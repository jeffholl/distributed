package controller;

import java.io.BufferedInputStream;
import java.util.Map.Entry;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JobManager implements Runnable {
	
	private static String echo = "JobManager";
	private static int NOJobs;
	private static int COMPLETEJobs;
	private static int UPLOADEDJobs;
	private static List<String> ipAddresses = new ArrayList<String>();
	private static HashMap<Integer, MasterWorker> mworkers=new HashMap<Integer, MasterWorker>();
	private int port;
	private int nextWorkerID;
	
	
	public JobManager(){
		NOJobs       = 0;
		COMPLETEJobs = 0;
		UPLOADEDJobs = 0;
		this.nextWorkerID = 0;
		
		//Do with arguments later
		port = 80;
	}

	@Override
	public void run() {
		//build socket connection		
		// Hard Coded port yet
		
		
		
		while(true){
			

			//Probably here also poll the workers - check their jobs etc.
			
			/*System.out.println(echo + " with " + NOJobs + " in system");

			try {
				TimeUnit.SECONDS.sleep(5);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
	}
	public String getEcho(){
		return this.echo;
	}
	public int getNumberOfJobs(){
		//TODO: Poll all workers and get all jobs and update Job Number
		return NOJobs;
	}
	public String getJarName(int workerID, int jobID){
		
		return mworkers.get(workerID).getJobs().get(jobID).getJarName();
	}
	public String getInputName(int workerID, int jobID){
		
		return mworkers.get(workerID).getJobs().get(jobID).getInputName();
		
	}
	public int getJobNumber(int workerID, int jobID){
		
		return mworkers.get(workerID).getJobs().get(jobID).getJobNumber();

	}
	public String getJobStatus(int workerID, int jobID){
		
		return mworkers.get(workerID).getJobs().get(jobID).getJobStatus();

	}
	
	public HashMap<Integer, MasterWorker> getMworkers() {
		return mworkers;
	}
	
	public void uploadJob(Job job){
		
		System.out.println("Upload New Job");
			if (mworkers.size()>0){
				System.out.println("we have a worker free");
				// create job on first server
				int leastUsed = getLeastUsedWorker();
				if(leastUsed!=-1){
					mworkers.get(leastUsed).createJob(job);
					System.out.println("job logges with worker with ID: "+leastUsed);
				}else{
					System.out.println("All servers Down, no job created");
				}
				
				
			} else {
				System.out.println("there are no servers");
			}
	}
	
	private int getLeastUsedWorker() {
		int noOfActiveJobs = 100000;
		int leastUsedWorkerID = -1;
		for(HashMap.Entry<Integer, MasterWorker> mworker : mworkers.entrySet()){
			if(mworker.getValue().getStatus() == "down"){
				continue;
			}
			int workerJobs = mworker.getValue().getNoOfJobs();
			if (workerJobs<noOfActiveJobs){
				noOfActiveJobs=workerJobs;
				leastUsedWorkerID=mworker.getKey();
			}
		}
		if(leastUsedWorkerID!=-1){
			return leastUsedWorkerID;
		}else{
			System.out.println("All workers set up in this Master are DOWN!");
			return -1;
		}
		}

	public void updateJobs() {
		for(int workerID = 0; workerID < mworkers.size(); workerID++) {
			mworkers.get(workerID).updateJobs();
		}
	}

	public void uploadIPAddress(String ipAddr) {
		if(!ipAddresses.contains(ipAddr)) {
			
			SocketFactory factory= SSLSocketFactory.getDefault();
			Socket socket = null;
			
			try {
				System.out.println("connecting to Worker at "+ipAddr+":"+port);
				socket = factory.createSocket(ipAddr, port);
				ipAddresses.add(ipAddr);
				MasterWorker mworker = new MasterWorker(this.nextWorkerID++, socket);
				mworkers.put(mworker.getWorkerID(), mworker);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
		}
	}
	public void uploadIPAddresses(List<String> ipAddrs){
		for(int i=0;i<ipAddrs.size();i++){
			if(!ipAddresses.contains(ipAddrs.get(i))) {
				
				SocketFactory factory= SSLSocketFactory.getDefault();
				Socket socket = null;
				
				try {
					System.out.println("connecting to Worker at "+ipAddrs.get(i)+":"+port);
					socket = factory.createSocket(ipAddrs.get(i), port);
					ipAddresses.add(ipAddrs.get(i));
					MasterWorker mworker =  new MasterWorker(this.nextWorkerID++, socket);
					mworkers.put(mworker.getWorkerID(),mworker);
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
				
			}
		}
	}
	public String getPath(int workerID, int jobID){
		return mworkers.get(workerID).getJobs().get(jobID).getPath();
	}
	public String getJobOutputName(int workerID, int jobID){
		return mworkers.get(workerID).getJobs().get(jobID).getOutputName();
	}
	
	public String getIPAddress(int workerID){
		return mworkers.get(workerID).getIPAddress();
	}
/*	public String getJobOutputName(int workerID,int jobID){
		String ipadd=mworkers.get(workerID).getIPAddress();
		int jID=getJobNumber(workerID, jobID);
		String name=mworkers.get(workerID).getJobs().get(jobID).getOutputName();
		String filename=ipadd+jID+name;
		return filename;
	}*/
	public void userCancelledJob(int workerID, int jobID){
		MasterWorker worker = mworkers.get(workerID);
		Job job = worker.getJobs().get(jobID);
		worker.cancelJob(job);
		job.deleteJob();
	}
	
	///TESTING PURPOSES
	public void Joshuatesting(int workerID, int jobID){
		Job job = mworkers.get(workerID).getJobs().get(jobID);
		job.setStatus(1);
		File file = new File(
			job.getPath() + 
			job.getDownload() + 
			File.separator + 
			buildOutPutFileName(job.getJarName()) 
		);
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		job.SetOutputComplete(buildOutPutFileName(job.getJarName()));
	
	}
	
	private String buildOutPutFileName(String _name){
		String[] parts = _name.split("\\.");
		return parts[0] + "-output.txt";
	}
	private File getJobJarFile(int workerID, int jobID){
		Job job = mworkers.get(workerID).getJobs().get(jobID);
		return new File(job.getPath() + job.getUpload() + File.separator + job.getJarName() );
	}
	private File getJobInputFile(int workerID, int jobID){
		Job job = mworkers.get(workerID).getJobs().get(jobID);
		return new File(job.getPath() + job.getUpload() + File.separator + job.getInputName() );
	}
	private void setJobOutputFile(File file, int workerID, int jobID){
		Job job = mworkers.get(workerID).getJobs().get(jobID);
		String outName = buildOutPutFileName(job.getJarName());
		File output = new File(outName);
		FileChannel src;
		FileChannel dest;
		try {
			src = new FileInputStream(file).getChannel();
			dest = new FileOutputStream(output).getChannel();
			dest.transferFrom(src, 0, src.size());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		job.SetOutputComplete(buildOutPutFileName(job.getJarName()));
	}
	public int getJobRamAllocation(int workerID, int jobID){
		return mworkers.get(workerID).getJobs().get(jobID).getRamAllocation();
	}
	public int getJobRunTime(int workerID, int jobID){
		return mworkers.get(workerID).getJobs().get(jobID).getRunTime();
	}
	public boolean spoolDownServer(Integer _server){
		if (mworkers.get(_server).shutDown()){
			ipAddresses.remove(mworkers.get(_server).getIPAddr());
			mworkers.remove(_server);
			return true; //shut down server
		} else {
			return false;
		}
													 //delete server on list
	}
	public void removeJob(Integer workerID, Integer jobID){
		Job job = mworkers.get(workerID).getJobs().get(jobID);
		mworkers.get(workerID).deleteJob(job);
		mworkers.get(workerID).removeJob(jobID);
	}
}
