

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.RequestContext;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;



import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.JsonArray;

import controller.Job;
import controller.JobManager;
import controller.MasterWorker;

/**
 * Servlet implementation class Index
 *  
 * A LIST OF SERVICES 
 * 
 * INIT() -> BUILD FILE STRUCTURE FOR UPLOAD DOWNLOAD
 *
 * DOGET  -> DELIVER ALL SYSTEM INFORMATION
 *		  -> ADD SINGLE IP ADDRESS
 *		  -> CANCEL JOBS
 *		  -> DOWNLOAD A FILE
 *
 * DOPOST -> UPLOAD A JOB
 * 		  -> UPLOAD AN IPSET CONTAINING IP ADDRESSES
 *
 *
 */
@WebServlet("/Index")
public class Index extends HttpServlet{
	
	 private static final String UPLOAD_DIRECTORY = "upload";		 //local instance storage
	 private static final String DOWNLOAD_DIRECTORY = "download";    //download storage directory 
	 private static final int THRESHOLD_SIZE     = 1024 * 1024 * 3;  // 3MB
	 private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
	 private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB
	 private static final long serialVersionUID = 1L;
	 private JobManager MANAGER = new JobManager();					 //build controller instance
	
	 /**
     * @throws IOException 
	 * @see HttpServlet#HttpServlet()
     */
    public void init(ServletConfig config){
       try {
    	   super.init(config);
	   } catch (ServletException e) {
			// TODO Auto-generated catch block
		   e.printStackTrace();
	   }
       // System.out.println("building socket");
       new Thread(MANAGER).start();	// start controller 
        						    // needs inject of IP address before beginning 
       BuildDirectories(getServletContext().getRealPath(""),UPLOAD_DIRECTORY,DOWNLOAD_DIRECTORY); //build server directory
      
    }
   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
    	
    	//Do get handles COMMANDS from ajax including
    	//getJobs -> will extend to get services information
    	//upload single IP address
  
    	response.setContentType("application/json");//set response type 
    	response.setCharacterEncoding("utf-8");
    	JSONObject send = new JSONObject();			//build response target
    	
    	String handler = request.getParameter("COMMAND");//find request purpose
 
    	
    	//DELIVER SYSTEM INFORMATION
    	if(handler.equals("getJobs")){ //update all local field variables 
    		getJobsInfo(send);		   //as JSONObjects can be expanded easily 
    								   //the extensions to server info will be 
    								   //straight forward 	
    		getServerInfo(send);
    		PrintWriter out = response.getWriter();		//output back to client window
    		out.println(send);
    	//ADD SINGLE IP ADDRESS TO SERVERS
    	} else if (handler.equals("addIPAddress")){ //upload single ip address
    		addIPAddress(request.getParameter("ipAddress"));
    		PrintWriter out = response.getWriter();		//output back to client window
    		out.println(send);
    	
    	//DOWNLOAD A SINGLE JOB FILE
    	} else if (handler.equals("downloadJob")){
    		
    		MasterWorker worker = MANAGER.getMworkers().get(Integer.parseInt(request.getParameter("workerID")));
    		Job job = worker.getJobs().get(Integer.parseInt(request.getParameter("jobNumber")));
    		File file = new File(job.getPath()+job.getDownload()+File.separator+job.getOutputName());
    		
 
    		response.setContentType("application/octet-stream");
    		response.setContentLength((int) file.length());
    		response.setHeader( "Content-Disposition",
    		         String.format("attachment; filename=\"%s\"", file.getName()));

    		OutputStream out = response.getOutputStream();
    		try (FileInputStream in = new FileInputStream(file)) {
    		    byte[] buffer = new byte[MAX_FILE_SIZE];
    		    int length;
    		    while ((length = in.read(buffer)) > 0) {
    		        out.write(buffer, 0, length);
    		    }
    		}
    		out.flush();
    	
    	//CANCEL A JOB
    	} else if(handler.equals("removeJob")){
    		System.out.println("removing job: "+request.getParameter("jobNumber"));
    		MANAGER.removeJob(
    			Integer.parseInt(request.getParameter("workerID")),
    			Integer.parseInt(request.getParameter("jobNumber"))
    		);
    		PrintWriter out = response.getWriter();		//output back to client window
    		out.println(send);
    		//DELIVER SYSTEM INFORMATION
    	
    	//CANCEL A JOB
    	} else if(handler.equals("cancelJob")){
    		System.out.println("cancelling job: "+request.getParameter("jobNumber"));
    		MANAGER.userCancelledJob(
    			Integer.parseInt(request.getParameter("workerID")),
    			Integer.parseInt(request.getParameter("jobNumber"))
    		);
    		PrintWriter out = response.getWriter();		//output back to client window
    		out.println(send);
    		//DELIVER SYSTEM INFORMATION
    	} else if(handler.equals("shutDownServer")){ 	//shut down server 
        		MANAGER.spoolDownServer(Integer.parseInt(request.getParameter("serverNumber")));
    			
        		PrintWriter out = response.getWriter();	//output back to client window
        		out.println(send);
    	//NOTHING
    	} else {
    		PrintWriter out = response.getWriter();		//output back to client window
    		out.println(send);
    	}
    }
	/**
	 * @throws IOException 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		//doPost handles all of the file retieval from client window - inclusive of 
		//jobs submissions
		//ip address submissions via <name>.ipset
		//final String path = upload.parseRequest(new ServletRequestContext(request));
		
		DiskFileItemFactory factory = new DiskFileItemFactory();//initialize blank template holder for properties of uploaded files 
		factory.setSizeThreshold(THRESHOLD_SIZE);// maximum size that will be stored in memory
		ServletFileUpload upload = new ServletFileUpload(factory);
		upload.setSizeMax(MAX_REQUEST_SIZE);
		upload.setSizeMax(MAX_FILE_SIZE);// maximum size before a FileUploadException will be thrown
		
		int runTime = 0;
		int ramAllocation = 0;
		
		try {	//try pulling information from request script
			String uploadPath = getServletContext().getRealPath("") + UPLOAD_DIRECTORY;
			List fileItems = upload.parseRequest(new ServletRequestContext(request));
			Iterator i = fileItems.iterator();
			List<File> files = new ArrayList<File>(); 
			while ( i.hasNext() ){
				FileItem fi = (FileItem)i.next();
				if ( !fi.isFormField() ){
		        	String fileName = new File(fi.getName()).getName();// Get the uploaded file parameters	
		        	String filePath = uploadPath + File.separator + fileName;
	                files.add(new File(filePath));
	                fi.write(new File(filePath));// saves the file on local instance
		        } else {
		        	if(fi.getFieldName().equals("runTime")){
		        		if(fi.getString().matches("^[0-9]+$")){
		        			runTime = Integer.parseInt(fi.getString())*60*1000;
		        		}
		        	}
		        	if(fi.getFieldName().equals("ramAllocation")){
		        		if(fi.getString().matches("^[0-9]+$")){
		        			ramAllocation = Integer.parseInt(fi.getString());
		        		}
		        	}
		        }
		    }
			//UPLOAD A SINGLE JOB
			if(files.size()==2){
				System.out.println("i am a single file");
				MANAGER.uploadJob(
					new Job( 
						files.get(0).getName(),
						files.get(1).getName(),
						0,
						getServletContext().getRealPath(""),
						UPLOAD_DIRECTORY,
						DOWNLOAD_DIRECTORY,
						runTime,
						ramAllocation
					)
				);
			
		    //UPLOAD A SET OF IP ADDRESSES 
			} else if(files.size()==1 && files.get(0).getName().contains(".ipset")){
				List<String> ip = new ArrayList<String>(); 
				try (BufferedReader br = new BufferedReader(new FileReader(files.get(0)))) {
					for(String line; (line = br.readLine()) != null; ) {
						if(line.matches("(^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$|localhost)")){
				    	  ip.add(line);
						}
				    }
					if(!ip.isEmpty()){
						MANAGER.uploadIPAddresses(ip);
					}
				} catch (Exception ex) {
					System.err.println("caught error: " + ex.getMessage());
				}
				ip = null;   //for garbage collector
			} else {
				System.out.println("also here");
			}
			files = null;	 //for garbage collector
			i = null;		 //for garbage collector
		    fileItems = null;//for garbage collector
		    
        } catch (Exception ex) {
            System.out.println("message: There was an error: " + ex.getMessage());
		} finally {
			factory = null;
			upload = null;	
			response.sendRedirect("/Master"); //send back to master
		}
	}
	private void getJobsInfo(JSONObject _send){
		MANAGER.updateJobs();
		JSONArray _jsonArray = new JSONArray();
    	
		HashMap<Integer, MasterWorker> mworkers = MANAGER.getMworkers();
    	System.out.println("number of workers "+mworkers.size());
    	
    	for(HashMap.Entry<Integer, MasterWorker> mworker : mworkers.entrySet()){
    		HashMap<Integer, Job> workerJobs = mworker.getValue().getJobs();
	    	System.out.println("number of jobs "+workerJobs.size());
    		System.out.println();
	    	for(HashMap.Entry<Integer, Job> jobEntry : workerJobs.entrySet()){
    			Job job = jobEntry.getValue();
    			JSONObject jo = new JSONObject();
        		jo.put("jarName",job.getJarName());	//jarName
        		jo.put("inputName",job.getInputName());//inputName
        		jo.put("jobNumber",job.getJobID());//jobNumber
        		jo.put("workerID", mworker.getValue().getWorkerID());//
        		jo.put("jobStatus",job.getJobStatus());//jobStatus
        		jo.put("jobRunTime",job.getRunTime());//jobStatus
        		jo.put("jobRamAllocation",job.getRamAllocation());//jobStatus
        		_jsonArray.add(jo);
    		}
    	}
    	_send.put("jobs",_jsonArray); 
	}
	private void addIPAddress(String string){
		if(string.matches("(^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$|localhost)")){
			MANAGER.uploadIPAddress(string);
		}
	}
	private void getServerInfo(JSONObject _send){
		MANAGER.updateJobs();
		JSONArray _jsonArray = new JSONArray();
		HashMap<Integer, MasterWorker> mworkers = MANAGER.getMworkers();

    	for(int workerID=0; workerID<mworkers.size(); workerID++) {
    		
    		MasterWorker mworker = mworkers.get(workerID);
    		
        	JSONObject jo = new JSONObject();
        	jo.put("serverAddress",mworker.getIPAddress());	//jarName
        	jo.put("serverJobs",mworker.getJobs().size());//inputName
        	jo.put("serverUsage",mworker.getUsage());//jobNumber
        	jo.put("serverStatus",mworker.getStatus());
        	_jsonArray.add(jo);
        }
        _send.put("servers",_jsonArray); 
	}
	private void BuildDirectories(String path,String upload, String download){
		//REMOVE FILES AND REMAKE DIRECTORY - UPLOAD
		File uploadDir = new File(path + File.separator + upload );
		if (!uploadDir.exists()) {
			uploadDir.mkdir();
		} else if(uploadDir.exists()){
			String[]entries = uploadDir.list();
			for(String s: entries){
			    File currentFile = new File(uploadDir.getPath(),s);
			    currentFile.delete();
			}
			uploadDir.delete();
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
			}
		}
		
		//REMOVE FILES AND REMAKE DIRECTORY - DOWNLOAD
		File downloadDir = new File(path + File.separator + download);
		if (!downloadDir.exists()) {
			downloadDir.mkdir();
		} else if(downloadDir.exists()){
			String[]entries = downloadDir.list();
			for(String s: entries){
			    File currentFile = new File(downloadDir.getPath(),s);
			    currentFile.delete();
			}
			downloadDir.delete();
			if (!downloadDir.exists()) {
				downloadDir.mkdir();
			}
		}
	}
}