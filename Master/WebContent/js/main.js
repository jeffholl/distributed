$(document).ready(
		function(){
		//initialize 
		var visableJobs = 0;	
		var visableServers = 0;
		doPoll();
			$('#clearFieldsJob').click(function(){
			    $("#jarFile").val("");
			    $("#inputFile").val("");
			    $("#runTime").val("");
			    $("#ramAllocation").val("");
			});	
			$('#clearFieldsIP').click(function(){
				$("#singleIPAddresses").val("")
			    $("#ipAddresses").val("");
			});
			function doPoll(){
				$.ajax({
					dataType : 'json', 
					contentType: 'application/json',
					type : "get",
					url : "Index",
					data : 'COMMAND=getJobs',
					success : function(msg) {
						buildJobs(msg);	
						buildServer(msg);
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
	                    //alert("Status: " + textStatus); alert("Error: " + errorThrown); 
					}
				});	
				setTimeout(doPoll, 8000);
			}	 
			function buildJobs(msg){
				
				//build job fields	
				var wrapper = "<h2>job status</h2><table class='table'>";
				wrapper += "<tr><td>job</td><td>jar</td><td>input</td><td>status</td><td>run time</td><td>ram allocation</td><td>action</td><td>download</td></tr>"
				for(i=0;i<msg.jobs.length;i++){
					wrapper += "<tr>";
					wrapper += "<td>"+ msg.jobs[i].jobNumber +"</td>";
					wrapper += "<td>"+ msg.jobs[i].jarName +"</td>";
					wrapper += "<td>"+ msg.jobs[i].inputName +"</td>";
					wrapper += "<td>"+ msg.jobs[i].jobStatus +"</td>";
					wrapper += "<td>"+ (msg.jobs[i].jobRunTime ? msg.jobs[i].jobRunTime / (60*1000) + ' mins' : '-') +"</td>";
					wrapper += "<td>"+ (msg.jobs[i].jobRamAllocation ? msg.jobs[i].jobRamAllocation + 'MB' : '-') +"</td>";
					wrapper += "<td>"+buildCancel(msg.jobs[i].jobStatus,msg.jobs[i].jobNumber,msg.jobs[i].workerID)+"</td>";
					wrapper += "<td>"+buildDownload(msg.jobs[i].jobStatus,msg.jobs[i].jobNumber,msg.jobs[i].workerID)+"</td>";
					wrapper += "</tr>";
				}
				wrapper +="</table>";
				if(visableJobs != msg.jobs.length && msg.jobs.length>0){
					$("#wrapper").html(wrapper);
					visableJobs = msg.jobs.length;
				} else if (msg.jobs.length>0){
					
				} else {
					$("#wrapperDiv").css("display","none");
				}
				
			}
			function buildServer(msg){
				var wrapper = "<h2>server status</h2><table class='table'>";
				wrapper += "<tr><td></td><td>ip address</td><td>status</td><td>#jobs</td><td>#usage</td><td>stop connection</td></tr>"
				for(i=0;i<msg.servers.length;i++){
					wrapper += "<tr>";
					wrapper += "<td><img src='/Master/img/server.png' width='20px'></td>";
					wrapper += "<td>"+ msg.servers[i].serverAddress +"</td>";
					wrapper += "<td>"+ msg.servers[i].serverStatus +"</td>";
					wrapper += "<td>"+ msg.servers[i].serverJobs +"</td>";
					wrapper += "<td>"+ msg.servers[i].serverUsage +"</td>";
					wrapper += "<td>"+ buildCancelSocket(i)+"</td>";
					wrapper += "</tr>";
				}
				wrapper +="</table>";	
				if(visableServers != msg.servers.length && msg.servers.length>0){
					$("#serverStatus").html(wrapper);
					visableServers = msg.servers.length;
				} else if (msg.msg.servers.length>0){
					
				} else {
					$("#serverStatus").css("display","none");	
				}
			}
			function buildCancelSocket(_int){
				cancelForm = "<a href='javascript:cancelSocket(";
				cancelForm += _int+"";
				cancelForm += ")' class='btn btn-default btn-xs distributed-stop'>shut down</a>";
				return cancelForm;
			}
			function buildCancel(_string,_int,_int2){
				if (_string == "running" || _string == "queued"){
					cancelForm = "<a href='javascript:cancelJob(";
					cancelForm += _int+","+_int2+"";
					cancelForm += ")' class='btn btn-default btn-xs distributed-cancel'>cancel</a>";
					return cancelForm;
				} else {
					removeForm = "<a href='javascript:removeJob(";
					removeForm += _int+","+_int2+"";
					removeForm += ")' class='btn btn-default btn-xs distributed-cancel'>remove</a>";
					return removeForm;
				}
				return "";
			}
			function buildDownload(_string,_int,_int2){
				if (_string == "cancelled"){
					downloadForm = "<a href='javascript:alert(\"job removed by user\")' class='btn btn-default btn-xs distributed-left'>report</a>";
					return downloadForm;
				} else if(_string == "download" | _string == "downloaderror"){
					downloadForm = "<form action='./Index' method='GET'>";
					downloadForm +="<input type='hidden' name='jobNumber' value='"+_int+"'>";
					downloadForm +="<input type='hidden' name='workerID' value='"+_int2+"'>";
					downloadForm +="<input type='hidden' name='COMMAND' value='downloadJob'>";
					downloadForm +="<button class='btn btn-default btn-xs distributed-left'>download</form>";
					return downloadForm;
				} else if(_string == "running"){
					downloadForm = "";
					return downloadForm;
				} else if(_string == "queued"){
					return downloadForm = "";
				} else {
					return downloadForm = "error: unknow error";
				}
				return "";
			}
			$("#submitSingleIP").submit(function(){
				$.ajax({
					dataType : 'json', 
					contentType: 'application/json',
					type : "get",
					url : "Index",
					data : 'COMMAND=addIPAddress&ipAddress=' + $("#singleIPAddresses").val(),
					success : function(msg) {
						$("#singleIPAddresses").val("")
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) { 
	                    //alert("Status: " + textStatus); alert("Error: " + errorThrown); 
					}
				});	
			});
		});
function removeJob(_int,_int2){
	$.ajax({
		dataType : 'json', 
		contentType: 'application/json',
		type : "get",
		url : "Index",
		data : 'COMMAND=removeJob&jobNumber='+_int+'&workerID='+_int2,
		success : function(msg) {
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
		}
	});	
};
function cancelJob(_int,_int2){
	$.ajax({
		dataType : 'json', 
		contentType: 'application/json',
		type : "get",
		url : "Index",
		data : 'COMMAND=cancelJob&jobNumber='+_int+'&workerID='+_int2,
		success : function(msg) {
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
		}
	});	
};
function cancelSocket(_int){
	$.ajax({
		dataType : 'json', 
		contentType: 'application/json',
		type : "get",
		url : "Index",
		data : 'COMMAND=shutDownServer&serverNumber='+_int,
		success : function(msg) {
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) { 
            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
		}
	});	
};