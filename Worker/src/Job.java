

import java.io.File;

public class Job {
	
	private File jar = null;
	private File input = null;
	private File output = null;
	private int status;
	private int jobNumber = 0;
	private Process process;
	private int RAM;
	private int runtime;
	private long startTime;
	private File error = null;
	
	public Job (File a, File b, int number){
		jar = a;
		input = b;
	    status = 0;
	    jobNumber= number;
	    runtime = 0;
	    startTime = 0;
	}	

	public void setStartTime(long i){
		this.startTime = i;
	}
	public long getStartTime(){
		return startTime;
	}
	public int getRuntime(){
		return runtime;
	}
	public void setRuntime(int i){
		this.runtime = i;
	}
	public void setError(File file){
		error = file;
	}
	public File getError(){
		return error;
	}
	public int getRamAllocation(){
		return RAM;
	}
	public void setRamAllocation(int ram){
		this.RAM = ram;
	}
	public void setProcess(Process p){
		process = p;
	}
	
	public Process getProcess(){
		return process;
	}
	public void setOutputFile(File file){
		output = file;
	}
	
	public void setJobNumber(int i){
		jobNumber = i;
	}
	public File getJar(){
		return jar;
	}
	public String getJarName(){
		return jar.getName();
	}
	public File getInput(){
		return input;
	}
	public String getInputName(){
		return input.getName();
	}
	public File getOutput(){
		return output;
	}
	public String getOutputName(){
		return output.getName();
	}
	public int getJobNumber(){
		return jobNumber;
	}
	public String getJobStatus(){
		String send;
		switch (status){
			case 0: send="queued";
					break;
			case 1: send="running";
					break;
			case 2: send="completed";
					break;
			case 3: send="cancelled";
			 		break;
			case 4: send="error";
					break;
			case 5: send="timeout";
					break;
			case 6: send="download";
					break;
			case 7: send="downloaderror";
					break;
			default:send="unknown";
			 		break;
		}	
		return send;
	}
	public void setStatus(String string){
		switch (string){
		case "queued": status=0 ;
				break;
		case "completed": status = 2;
				break;
		case "running": status = 1;
				break;
		case "cancelled": status = 3;
		 		break;
		case "error": status = 4;
				break;
		case "timeout": status = 5;
				break;
		case "download": status = 6;
				break;
		case "downloaderror": status = 7;
				break;
		default:
		 		break;
	}	
	}
}
