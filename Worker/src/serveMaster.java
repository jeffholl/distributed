import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import java.util.ArrayList;
import java.util.List;



import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class serveMaster implements Runnable {

	Socket socket;
	private static List<Job> jobs = new ArrayList<Job>();
	private static int jobCounter = 0;
	//Hard code Filesize - maybe adapt? 100mb failing at 500mb
	public final static int FILE_SIZE = 100223869;
	
	
	serveMaster(Socket s){
		socket = s;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try (Socket workerSocket = socket) {
			System.out.println("connected to Master");
			DataInputStream in = new DataInputStream(workerSocket.getInputStream());
			DataOutputStream out = new DataOutputStream(workerSocket.getOutputStream());
			
			while(true){
				/*
				Get jobs from Master and execute them
				 * Just always receive messages from the master and act accordingly
				 * To update Job statuses etc we tell the Jobnumber between Master and Worker
				 */
				System.out.println("Waiting to read message in Switch");
				String message = in.readUTF();
				System.out.println("Message from master received before Switch");
				JSONParser parser = new JSONParser();
				JSONObject messageJSON = (JSONObject) parser.parse(message);
				
				System.out.println("Message received: " + messageJSON.get("Type").toString());
				switch (messageJSON.get("Type").toString()) {
					
				case "STATUS":
					sendStatus(workerSocket);
					break;
				case "GET-JOBS":
				// Respond with list of current registered Jobs and "required information to request the controls on the job (jobID and valid actions)" -> what?
					System.out.println("Master wants GET JOBS");
					updateJobStatuses(workerSocket);
					System.out.println("updated job statuses in switch");
					getJobs(workerSocket);
					System.out.println("got Jobs (switch)");
					break;
					
				case "GET-JOB-DATA":
				// Respond with the output of the job
					//Assuming Master send the job-ID in the JSON Message under Job-ID:
					int JobID = Integer.valueOf(messageJSON.get("JOB-ID").toString());
					getJobOutput(workerSocket, JobID);
					break;
					
				
				case "CREATE-JOB":
					System.out.println("Worker received Create Job command");
					
					// I suggest after this message the Master will send the jar-File and the input and output-Files
					// So the possibility of receiving these Files in this switch-statement should not happen	
					// I guess we use the "Jobnumber" as the unique ID for the jobs (to know which one is which on both sides)
					// Assuming Master sends filenames as well
					
					createJob(workerSocket, 
							messageJSON.get("JAR-FILENAME").toString(), 
							messageJSON.get("INPUT-FILENAME").toString(),
							Integer.valueOf(messageJSON.get("JAR-SIZE").toString()),
							Integer.valueOf(messageJSON.get("INPUT-SIZE").toString()),
							Integer.valueOf(messageJSON.get("MAX-RAM").toString()),
							Integer.valueOf(messageJSON.get("DEADLINE").toString()));
					
					
					System.out.println("Create Job completed");
					break;
				case "CANCEL-JOB":
					System.err.println("Cancel Job message receved");
					int jobID = Integer.valueOf(messageJSON.get("JOB-ID").toString());
					cancelJob(jobID);
					break;
				case "DELETE-JOB":
					//Assuming Master send the job-ID in the JSON Message under Job-ID:
					int jobIDD = Integer.valueOf(messageJSON.get("JOB-ID").toString());
					deleteJob(workerSocket, jobIDD);
					break;
				default:
					//Something went wrong.
					System.err.println("Message Type not recognized by Worker in Switch Statement");
					break;
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("IO Exception in Switch statement");
			e.printStackTrace();
		} catch (org.json.simple.parser.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	
	}

	private static void sendStatus(Socket workerSocket) {
		System.out.println("Worker in sendStatus");
		try {
			DataOutputStream out = new DataOutputStream(workerSocket.getOutputStream());
			DataInputStream in = new DataInputStream(workerSocket.getInputStream());
			
			JSONObject sendStatus = new JSONObject();
			sendStatus.put("Type", "Alive");
			out.writeUTF(sendStatus.toJSONString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		
		
	}

	private static synchronized void cancelJob(int jobID) {
		for(int i = 0; i<jobs.size(); i++){
			if(jobs.get(i).getJobNumber()==jobID){
				//Terminate process & remove from lists
				jobs.get(i).getProcess().destroy();
				jobs.get(i).setStatus("cancelled");
				System.err.println("Job Cancelled");
				break;
			}
		}
		
	}

	/**
	 * updates the statuses of all the jobs.
	 */
	private static synchronized void updateJobStatuses(Socket workerSocket) {
		for(int i = 0; i<jobs.size(); i++){
			Job job = jobs.get(i);
			String jobStatus = job.getJobStatus();
			
			//Only check running jobs because others should be updated automatically
			System.out.println("Checking job status for Job ID "+ jobs.get(i).getJobNumber() + ", Status = " + jobs.get(i).getJobStatus());
			
			if (jobStatus.matches("running")){
				
				boolean  running = isProcessRunning(job.getProcess());
				if (running == false){
					//If process completed, check if exitvalue normal.
					if (job.getProcess().exitValue() == 0){
						jobs.get(i).setStatus("completed");
					} else {
						jobs.get(i).setStatus("error");
					}
				} else {
					jobs.get(i).setStatus("running");
				}
			}
			//check for timeout
			if(job.getRuntime()>0){
			if (jobs.get(i).getJobStatus().matches("running") && System.currentTimeMillis()-job.getStartTime() > job.getRuntime()){
				outOfTime(workerSocket, job.getJobNumber());
			}
			}
			
			
		}
		
	}
	
	public static boolean isProcessRunning(Process process) 
	{
	    try 
	    {
	        process.exitValue();
	        return false;
	    } 
	    catch(IllegalThreadStateException e) 
	    {
	        return true;
	    }
	}

	private static synchronized void deleteJob(Socket workerSocket, int jobID) {
		//I don't know how to find the Job in the jobs list without iterating through it...
		for(int i = 0; i<jobs.size(); i++){
			if(jobs.get(i).getJobNumber()==jobID){
				//Terminate process & remove from lists
				jobs.get(i).getProcess().destroy();
				jobs.remove(i);
				System.err.println("Job deleted! Fuck yeah");
				return;
			}
		}
		System.out.println("Job not found in this worker. No job deleted");
		
	}
	
	private static synchronized void outOfTime(Socket workerSocket, int jobID) {
		//I don't know how to find the Job in the jobs list without iterating through it...
		for(int i = 0; i<jobs.size(); i++){
			if(jobs.get(i).getJobNumber()==jobID){
				//Terminate process & remove from lists
				jobs.get(i).getProcess().destroy();
				jobs.get(i).setStatus("timeout");
				break;
			}
		}
		
	}

	private static void createJob(Socket workerSocket, String jarName, String inputName, int jarSize, int inputSize, int RAM, int runtime) {
		
		try {
			System.out.println("Worker in createJob");
			DataInputStream in = new DataInputStream(workerSocket.getInputStream());
			DataOutputStream out = new DataOutputStream(workerSocket.getOutputStream());
			
			
		    int JobID = getNextJobID();
			//Request Jar File of Job
			JSONObject requestJar = new JSONObject();
			
			//Worker requests Jar and sends Job ID
			requestJar.put("Type", "sendJar");
			requestJar.put("JOB-ID", JobID);
			
			System.out.println("Worker about to request Jar File");
			out.writeUTF(requestJar.toJSONString());
			out.flush();
			
			System.out.println("Trying to receive Jar File");
			//in.readUTF();
			File jarFile = getFile(workerSocket, JobID, jarName, jarSize);
			System.out.println("Received jar File");
			
			JSONObject requestInput = new JSONObject();
			requestInput.put("Type", "sendInput");
			out.writeUTF(requestInput.toJSONString());
			out.flush();
			
			//In order to wait till master sends
			//in.readUTF();
			File inputFile = getFile(workerSocket, JobID, inputName, inputSize);
			System.out.println("Input File Received");
			
			Job job = new Job(jarFile, inputFile, JobID);
			job.setRamAllocation(RAM);
			job.setRuntime(runtime);
			job.setStatus("queued");
			
			
			
			/*
			 * List of jobs and processes should have same indexes. Is this dangerous?
			 */
			System.out.println("Just before job execution");
			
			//So, I hope this attaches the process to the job and sets the outputfile to the job.
			job.setOutputFile(executeNewJob(job));
			job.setStatus("running");
			addJob(job);
			System.out.println("Process for Job started. Job ID: "+JobID);

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private static synchronized void addJob(Job job) {
		jobs.add(job);
		
	}

	private synchronized static int getNextJobID() {
		jobCounter++;
		return jobCounter;
	}

	/**
	 * Like in the following Tutorial - have to implement it samewise on Masterside
	 * http://www.rgagnon.com/javadetails/java-0542.html
	 */
	private static File getFile(Socket workerSocket, int jobID, String fileName, int filesize) throws IOException {
		
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		File received = new File(jobID+fileName);
		
		try {
			int bytesRead;
			int current = 0;
			InputStream is;
			
			is = workerSocket.getInputStream();
			byte [] mybytearray  = new byte [FILE_SIZE];
			fos = new FileOutputStream(received);
		    bos = new BufferedOutputStream(fos);
		    bytesRead = is.read(mybytearray,0,mybytearray.length);
		    current = bytesRead;
		    System.out.println("First read of File Complete");
		    
		      while(current<filesize) {
		         bytesRead = is.read(mybytearray, current, (mybytearray.length-current));
		         if(bytesRead >= 0) current += bytesRead;
		         System.out.println("Do-loop entered and bytes read"+bytesRead);
		      } 
	
		      bos.write(mybytearray, 0 , current);
		      bos.flush();
		     
	      
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if (fos != null) fos.close();
		    if (bos != null) bos.close();
		}
	      return received;
	}

	private static void getJobOutput(Socket workerSocket, int jobID) throws IOException {
		//Update statusses in case job ended now.
	
		updateJobStatuses(workerSocket);
		Job job = findJob(jobID);
		
			
			DataInputStream in = new DataInputStream(workerSocket.getInputStream());
			DataOutputStream out = new DataOutputStream(workerSocket.getOutputStream());
			JSONObject acknowledgment = new JSONObject();
			System.out.println("Trying to get job output. job ID: "+jobID + "Job Status: "+ job.getJobStatus());
			
			
			if (job.getJobStatus().matches("running") || job.getJobStatus().matches("queued")){
				System.out.println("Can't send outputFile bc job not completed");
				acknowledgment.put("Type", "JOB-RUNNING");
				out.writeUTF(acknowledgment.toJSONString());
				
			} else if (!job.getJobStatus().matches("completed")){
				acknowledgment.put("Type","ERROR");
				acknowledgment.put("FILESIZE", job.getError().length());
				out.writeUTF(acknowledgment.toJSONString());
				
				FileInputStream fis = null;
			    BufferedInputStream bis = null;
			    OutputStream os = null;
			    
				
				    
				    //I followed simple naming conventions for the files, maybe we can optimize this
				    File error = job.getError();
			        byte [] mybytearray  = new byte [(int) error.length()];
			        fis = new FileInputStream(error);
			        bis = new BufferedInputStream(fis);
			        bis.read(mybytearray,0,mybytearray.length);
			        os = workerSocket.getOutputStream();
			        os.write(mybytearray,0,mybytearray.length);
			        os.flush();
			        System.out.println("Done with sending ERROR File");
			        

					job.setStatus("downloaderror");
			        //Close the things
			        bis.close();
				
			}else{
				acknowledgment.put("Type","TRANSMITTING-OUTPUT-FILE");
				acknowledgment.put("FILESIZE", job.getOutput().length());
				out.writeUTF(acknowledgment.toJSONString());
				System.out.println("Job Complete - Sending the Output File to Master. Job ID: "+job.getJobNumber());
			FileInputStream fis = null;
		    BufferedInputStream bis = null;
		    OutputStream os = null;
		    
			
			    
			    //I followed simple naming conventions for the files, maybe we can optimize this
			    File output = job.getOutput();
		        byte [] mybytearray  = new byte [(int) output.length()];
		        fis = new FileInputStream(output);
		        bis = new BufferedInputStream(fis);
		        bis.read(mybytearray,0,mybytearray.length);
		        os = workerSocket.getOutputStream();
		        os.write(mybytearray,0,mybytearray.length);
		        os.flush();
		        System.out.println("Done with sending File");
		        

				job.setStatus("download");
		        //Close the things
		        bis.close();
		        
			}
			
			
		
	}
	private static synchronized Job findJob(int jobID) {
		// TODO Auto-generated method stub
		for(int i = 0; i<jobs.size(); i++){
			if(jobs.get(i).getJobNumber()==jobID){
				return jobs.get(i);} 
			
		
	}
		System.out.println("Job not found, error...");
		Job job = null;
		return job;
	}
	private static void getJobs(Socket workerSocket) {

		JSONObject jobInformation = new JSONObject();
		jobInformation.put("Type", "JOBS-ON-WORKER");
		
		jobInformation = addStatuses(jobInformation);
		
			try {
				DataOutputStream out = new DataOutputStream(workerSocket.getOutputStream());
				out.writeUTF(jobInformation.toJSONString());
				out.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		
	private static synchronized JSONObject addStatuses(JSONObject jobInformation) {
		// TODO Auto-generated method stub
		JSONArray listOfJobIDs = new JSONArray();
		JSONArray listOfJobStatuses = new JSONArray();
		if(jobs != null){
			for(int i=0;i<jobs.size();i++){
				listOfJobIDs.add(jobs.get(i).getJobNumber());
				listOfJobStatuses.add(jobs.get(i).getJobStatus());
			}
						
			
	}
		if(jobs!=null){
		jobInformation.put("IDs", listOfJobIDs);
		jobInformation.put("Statuses", listOfJobStatuses);
		}
		return jobInformation;

	}

	/**
	 * Execute a job
	 * Can this method be static? Java wanted me to.
	 * reuturns the output File of the Job (or null if Failed)
	 * 
	 * @param job
	 * @throws IOException 
	 */
	private static File executeNewJob(Job job) throws IOException{
				
		File OutPutFile = new File("Output "+job.getJobNumber()+".dat");
		ProcessBuilder jobProcessBuilder;
		//Old. Hoping Java knows what it's doing.
//		if (job.getRamAllocation()==0){
//			jobProcessBuilder = new ProcessBuilder("java", "-jar", job.getJar().getAbsolutePath().replaceAll("\\\\","\\\\\\\\") +"\"", "\"" + job.getInput().getAbsolutePath().replaceAll("\\\\","\\\\\\\\") + "\"", "\"" + OutPutFile.getAbsolutePath().replaceAll("\\\\","\\\\\\\\")+"\"");
//		}else{
//			jobProcessBuilder = new ProcessBuilder("java", "-Xmx"+job.getRamAllocation()+"M", "-jar", "\"" + job.getJar().getAbsolutePath().replaceAll("\\\\","\\\\\\\\") +"\"", "\"" + job.getInput().getAbsolutePath().replaceAll("\\\\","\\\\\\\\") + "\"", "\"" + OutPutFile.getAbsolutePath().replaceAll("\\\\","\\\\\\\\")+"\"");
//
//		}
		if (job.getRamAllocation()==0){
			//jobProcessBuilder = new ProcessBuilder("java", "-jar", "\"" + job.getJar().getAbsolutePath() +"\"", "\"" + job.getInput().getAbsolutePath() + "\"", "\"" + OutPutFile.getAbsolutePath()+"\"");
			jobProcessBuilder = new ProcessBuilder("java", "-jar", job.getJar().getAbsolutePath(), job.getInput().getAbsolutePath(), OutPutFile.getAbsolutePath());
		}else{
			//jobProcessBuilder = new ProcessBuilder("java", "-Xmx"+job.getRamAllocation()+"M", "-jar", "\"" + job.getJar().getAbsolutePath() +"\"", "\"" + job.getInput().getAbsolutePath() + "\"", "\"" + OutPutFile.getAbsolutePath()+"\"");
			jobProcessBuilder = new ProcessBuilder("java", "-Xmx"+job.getRamAllocation()+"M", "-jar", job.getJar().getAbsolutePath(), job.getInput().getAbsolutePath(),OutPutFile.getAbsolutePath());

		}
		File ErrorFile = new File("Error"+job.getJobNumber()+".txt");
		job.setError(ErrorFile);
		ErrorFile.createNewFile();
		jobProcessBuilder.redirectError(ErrorFile);
		
		try {
			
			//Like this we don't know yet which process is where in the processList. Maybe same lists as the job-list? Might cause errors.
			job.setStartTime(System.currentTimeMillis());
			Process jobProcess = jobProcessBuilder.start();
			job.setProcess(jobProcess);
			
			
		} catch (IOException e) {
			// TODO somehow return the error to the Master
			e.printStackTrace();
		}
		
		return OutPutFile;
	}
}
