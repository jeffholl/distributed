/**
 * 
 */

/**
 * @author Alex
 *
 */


import java.io.IOException;

import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;


public class Worker {
	
	private static int port;
	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * Worker accepts connection from masters
		 * Give the port as parameter -p 'port'
		 */
		
		port = Integer.valueOf(args[1]);
		ServerSocketFactory factory = SSLServerSocketFactory.getDefault();	
		try (ServerSocket worker = factory.createServerSocket(port)) {		
			System.out.println("started worker on port :"+port);
			while(true){
				Socket socket = worker.accept();
				//Thread t = new Thread(() -> serveMaster(socket));
				Thread t = new Thread(new serveMaster(socket));
				t.setDaemon(true);
				t.start();
			}
	} catch (IOException e) {
		e.printStackTrace();
		System.err.println("IO Exception");}
	}
	

		
	

}


